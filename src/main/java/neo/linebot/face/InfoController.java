/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.linebot.face;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author krisada
 */
@RestController
public class InfoController {

    @GetMapping(value = "/info", produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> info() {
        StringBuilder h = new StringBuilder();
        h.append("<!DOCTYPE html>\n");
        h.append("<html>\n");
        h.append("<head>\n");
        h.append("<title>App Infomation</title>");
        h.append("</head>\n");
        h.append("<body>\n");
        h.append("<h2>App Name : Line Face</h2>\n");
        h.append("<h3>Version : 1.0</h3>\n");
        h.append("<p>Description : This App for line provisioning face recogition and face app.</p>\n");
        h.append("</body>\n");
        h.append("</html>");
        return new ResponseEntity(h.toString(), HttpStatus.OK);
    }

    @GetMapping(value = "/faces", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseEngine> imgAll() {
        ResponseEngine res = new ResponseEngine();
        res.setCode(200);
        res.setMessage("SUCCESS");
        List ls = new ArrayList();
        try {
            String uri = "/tmp";
            File dir = new File(uri);
            File[] fs = dir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.getName().toLowerCase().endsWith(".jpg");
                }
            });
            for (int i = 0; i < fs.length; i++) {
                ls.add(fs[i].getName());
            }
        } catch (Exception ex) {
            res.setCode(500);
            res.setMessage("Load File All fail.");
        }
        res.setData(ls);
        return new ResponseEntity(res, HttpStatus.OK);
    }

    @GetMapping(value = "/face/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Resource> image(@PathVariable("id") String id) throws IOException {
        final ByteArrayResource inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get(
                "/tmp/"+id+".jpg"
        )));
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentLength(inputStream.contentLength())
                .body(inputStream);

    }
}
