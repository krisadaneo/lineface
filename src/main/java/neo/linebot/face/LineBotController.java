/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.linebot.face;

import com.google.common.io.ByteStreams;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.MessageContentResponse;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.ImageMessageContent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.*;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import java.io.File;

import lombok.NonNull;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@LineMessageHandler
public class LineBotController {

    private static Logger log = LoggerFactory.getLogger(LineBotController.class);

    @Autowired
    private LineMessagingClient lineMessagingClient;
    
    @Autowired

    @EventMapping
    public void handleTextMessage(MessageEvent<TextMessageContent> event) {
        log.info(event.toString());
        TextMessageContent message = event.getMessage();
        handleTextContent(event.getReplyToken(), event, message);
    }

    @EventMapping
    public void handleStickerMessage(MessageEvent<StickerMessageContent> event) {
        log.info(event.toString());
        StickerMessageContent message = event.getMessage();
        reply(event.getReplyToken(), new StickerMessage(
                message.getPackageId(), message.getStickerId()
        ));
    }

    @EventMapping
    public void handleLocationMessage(MessageEvent<LocationMessageContent> event) {
        log.info(event.toString());
        LocationMessageContent message = event.getMessage();
        reply(event.getReplyToken(), new LocationMessage(
                (message.getTitle() == null) ? "Location replied" : message.getTitle(),
                message.getAddress(),
                message.getLatitude(),
                message.getLongitude()
        ));
    }

    @EventMapping
    public void handleImageMessage(MessageEvent<ImageMessageContent> event) {
        log.info("EVENT MESSAGE:" + event.toString());
        ImageMessageContent content = event.getMessage();
        String replyToken = event.getReplyToken();
        log.info(("REPLY TOKEN:" + replyToken));
        log.info("Content ID:" + content.getId());

        try {
            //LineMessagingClient
            //final LineMessagingClient lineMessagingClient2 = LineMessagingClient..builder(event.getReplyToken()).build();
            //LineMessagingClient lineMessagingClient2 = LineMessagingClient.builder("ra+B8OfcSb4BKib9sXCv1GlrriBJh+U+DfU49phoVHcRf26hfaKFBX53B4HK2f/C+2Hv2r9cRE/FgrMhRlsQcx5EfxH17s24Qra26TqlDsDybzJKoDLMJ99iUdzvvWieiuYor4dhkhLDLjiwtKP89gdB04t89/1O/w1cDnyilFU=").build();
            //MessageContentResponse response = lineMessagingClient2.getMessageContent(content.getId()).get();
            //MessageContentResponse response = lineMessagingClient.getMessageContent(content.getId()).get();
            //DownloadedContent jpg = saveContent("jpg", getDataContent(content.getId()));
            //DownloadedContent previewImage = createTempFile("jpg");

            //system("convert", "-resize", "240x",
            //        jpg.path.toString(),
            //        previewImage.path.toString());
            //reply(replyToken, new ImageMessage(jpg.getUri(), previewImage.getUri()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("https://api-data.line.me/v2/bot/message/" + content.getId() + "/content");
            request.setHeader("Authorization", "Bearer ra+B8OfcSb4BKib9sXCv1GlrriBJh+U+DfU49phoVHcRf26hfaKFBX53B4HK2f/C+2Hv2r9cRE/FgrMhRlsQcx5EfxH17s24Qra26TqlDsDybzJKoDLMJ99iUdzvvWieiuYor4dhkhLDLjiwtKP89gdB04t89/1O/w1cDnyilFU=");
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();

            int responseCode = response.getStatusLine().getStatusCode();

            log.info("Request Url: " + request.getURI());
            log.info("Response Code: " + responseCode);
            String uri = "/tmp";
            log.info("Dir URI: " + uri);
            if (responseCode == 200) {

            }
            String userId = event.getSource().getUserId();

            InputStream inputStream = entity.getContent();

            OutputStream outputStream = Files.newOutputStream(Paths.get(uri + File.separator + userId + ".jpg"));
            ByteStreams.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
            client.close();
            if (userId != null) {
                lineMessagingClient.getProfile(userId)
                        .whenComplete((profile, throwable) -> {
                            if (throwable != null) {
                                this.replyText(replyToken, throwable.getMessage());
                                return;
                            }
                            /*
                            this.reply(replyToken, Arrays.asList(
                                    new TextMessage("Display name: " + profile.getDisplayName()),
                                    new TextMessage("Status message: " + profile.getStatusMessage()),
                                    new TextMessage("User ID: " + profile.getUserId())
                            ));*/
                            this.reply(replyToken, new TextMessage("https://docs.google.com/forms/d/e/1FAIpQLSf6GU9BRSv4FKx4nuOB0CH4C0xj5nWMMqGD4IqRjboG0xe7vQ/viewform?usp=sf_link"));
                            
                        });
            }
        } catch (Exception e) {
            reply(replyToken, new TextMessage("Cannot get image: " + content));
            e.printStackTrace();
        }

    }

//     public InputStream getDataContent(String id) {
//        try {
//            CloseableHttpClient client = HttpClientBuilder.create().build();
//            HttpGet request = new HttpGet("https://api-data.line.me/v2/bot/message/"+id+"/content");
//            request.setHeader("Authorization", "Bearer ra+B8OfcSb4BKib9sXCv1GlrriBJh+U+DfU49phoVHcRf26hfaKFBX53B4HK2f/C+2Hv2r9cRE/FgrMhRlsQcx5EfxH17s24Qra26TqlDsDybzJKoDLMJ99iUdzvvWieiuYor4dhkhLDLjiwtKP89gdB04t89/1O/w1cDnyilFU=");
//            HttpResponse response = client.execute(request);
//            HttpEntity entity = response.getEntity();
// 
//            int responseCode = response.getStatusLine().getStatusCode();
// 
//            log.info("Request Url: " + request.getURI());
//            log.info("Response Code: " + responseCode);
// 
//            InputStream inputStream = entity.getContent();
// 
//            try (OutputStream outputStream = Files.newOutputStream(tempFile.path)) {
//            ByteStreams.copy(inputStream, outputStream);
//            
//        } catch (IOException e) {
//            throw new UncheckedIOException(e);
//        }
// 
//            //is.close();
//            //fos.close();
// 
//            //client.close();
//            System.out.println("File Download Completed!!!");
//            return is;
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (UnsupportedOperationException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
    private void handleTextContent(String replyToken, Event event, TextMessageContent content) {
        String text = content.getText();

        log.info("Got text message from %s : %s", replyToken, text);

        switch (text) {
            case "Profile": {
                String userId = event.getSource().getUserId();
                if (userId != null) {
                    lineMessagingClient.getProfile(userId)
                            .whenComplete((profile, throwable) -> {
                                if (throwable != null) {
                                    this.replyText(replyToken, throwable.getMessage());
                                    return;
                                }
                                this.reply(replyToken, Arrays.asList(
                                        new TextMessage("Display name: " + profile.getDisplayName()),
                                        new TextMessage("Status message: " + profile.getStatusMessage()),
                                        new TextMessage("User ID: " + profile.getUserId())
                                ));
                            });
                }
                break;
            }
            default:
                log.info("Return echo message %s : %s", replyToken, text);
                this.replyText(replyToken, text);
        }
    }

    private void handleStickerContent(String replyToken, StickerMessageContent content) {
        reply(replyToken, new StickerMessage(
                content.getPackageId(), content.getStickerId()
        ));
    }

    private void replyText(@NonNull String replyToken, @NonNull String message) {
        if (replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken is not empty");
        }

        if (message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "...";
        }
        this.reply(replyToken, new TextMessage(message));
    }

    private void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        try {
            BotApiResponse response = lineMessagingClient.replyMessage(
                    new ReplyMessage(replyToken, messages)
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private void system(String... args) {
        ProcessBuilder processBuilder = new ProcessBuilder(args);
        try {
            Process start = processBuilder.start();
            int i = start.waitFor();
            log.info("result: {} => {}", Arrays.toString(args), i);
        } catch (InterruptedException e) {
            log.info("Interrupted", e);
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static DownloadedContent saveContent(String ext, MessageContentResponse response) {
        //log.info("Content-type: {}", response);
        DownloadedContent tempFile = createTempFile(ext);
        try (OutputStream outputStream = Files.newOutputStream(tempFile.path)) {
            ByteStreams.copy(response.getStream(), outputStream);
            log.info("Save {}: {}", ext, tempFile);
            return tempFile;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static DownloadedContent saveContent(String ext, InputStream inputStream) {
        //log.info("Content-type: {}", response);
        DownloadedContent tempFile = createTempFile(ext);
        try (OutputStream outputStream = Files.newOutputStream(tempFile.path)) {
            ByteStreams.copy(inputStream, outputStream);
            log.info("Save {}: {}", ext, tempFile);
            return tempFile;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static DownloadedContent createTempFile(String ext) {
        String fileName = LocalDateTime.now() + "-" + UUID.randomUUID().toString() + "." + ext;
        Path tempFile = FaceApplication.downloadedContentDir.resolve(fileName);
        tempFile.toFile().deleteOnExit();
        return new DownloadedContent(tempFile, createUri("/downloaded/" + tempFile.getFileName()));

    }

    private static String createUri(String path) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(path).toUriString();
    }

    @Value
    public static class DownloadedContent {

        Path path;
        String uri;
    }
}
